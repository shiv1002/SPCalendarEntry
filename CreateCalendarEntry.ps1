﻿Clear-Host
Add-Type -Path "C:\Program Files\Common Files\microsoft shared\Web Server Extensions\15\ISAPI\Microsoft.SharePoint.Client.dll"  
Add-Type -Path "C:\Program Files\Common Files\microsoft shared\Web Server Extensions\15\ISAPI\Microsoft.SharePoint.Client.Runtime.dll"  
function readSettingFile()
{
	 [xml]$settingContent = (Get-Content $inputFile -ErrorAction Inquire)
     [System.Xml.XmlElement]$root = $settingContent.get_DocumentElement()
     [System.Xml.XmlElement]$settings = $root.Setting
     [System.Xml.XmlElement]$setting = $null
     $Global:WebURL = $settings.WebURL
	 $Global:UserAccount = $settings.UserAccount
	 $Global:Password = $settings.Password
     $Global:ListName = $settings.ListName
     $Global:DataFile = $settings.DataFile
}  
$folder = [System.IO.Path]::GetDirectoryName($myInvocation.MyCommand.Definition)
$inputFile = "$folder\Settings.xml"
readSettingFile 
#$creds = Get-Credential
$creds = New-Object -TypeName System.Management.Automation.PSCredential -argumentlist $Global:UserAccount, $(convertto-securestring $Global:Password -asplaintext -force)
$ctx = New-Object Microsoft.SharePoint.Client.ClientContext($Global:WebURL)  
$ctx.credentials = $creds  
try{  
    $lists = $ctx.web.Lists  
    $list = $lists.GetByTitle($Global:ListName)  
	$entries = Get-Content "$folder\$Global:DataFile"
	foreach($entry in $entries)
	{
		$listItemInfo = New-Object Microsoft.SharePoint.Client.ListItemCreationInformation  
    	$listItem = $list.AddItem($listItemInfo)  
    	$listItem["Title"] = $entry.Split(",")[0]
		$listItem["Location"] = $entry.Split(",")[1]
		$listItem["EventDate"] = $entry.Split(",")[2]
		$listItem["EndDate"] = $entry.Split(",")[3]
    	$listItem.Update()      
    	$ctx.load($list)      
    	$ctx.executeQuery()  
    	Write-Host "Item Added with Title - " $entry.Split(",")[0]      
	}
}  
catch{  
    write-host "$($_.Exception.Message)" -foregroundcolor red  
}  